---
slug: /pitching-to-publishers
date: 2020-10-12
title: Pitching to Publishers
author: Jad Sarout
tag: Team
hero_image: library.jpg
published: true
summary: At some point in your gamedev journey, you may want to consider approaching publishers. When you do, you will need to present your game. How do you do that?
---
import More from '../../src/components/MoreInfo'

At some point in your gamedev journey, you may want to consider approaching publishers 
(<More title="Do I need a publisher?">
Tough question. 
Common wisdom says yes, if you want to reach a certain amount of sales. Marketing your game takes a lot of time, effort, and resources.
A publisher will be able to handle this while you focus on the game. Even if you don't want a publisher, conversations with them may help you situate yourself in the market. </More>).

When you do, you will need to present your game. Any format works, but we recommend a slide deck which allows a visual and clear presentation.

This article will be split in three sections:

- _How_ can you make your slides ([slides?](#how-can-i-make-slides))
- _What_ to include in your publisher's deck ([content?](#what-should-the-content-be))

First, let's present the deck we made for Transmogrify; then, we'll explain it.

<div style="position:relative;width:100%;height:0;padding-bottom:calc(56.25% + 40px);"><iframe allowfullscreen style="position:absolute; width: 100%; height: 100%;border: solid 1px #333;" src="https://www.beautiful.ai/embed/-MIPP5ZjGtoDKnEhfq-B?utm_source=beautiful_player&utm_medium=embed&utm_campaign=-MIPLOr3iH8IrOeA4Poe"></iframe></div>

# How Can I Make Slides?

This article is addressed to small-ish teams, like ours. Your artists could style and build slides from scratch, but that probably isn't ideal.

Making good looking slides is important, however. 
(<More>Whether we recognize or not, polish, or "the amount of quality we perceive in a product" inspires confidence. 
Publishers (and everyone else) will treat you with more respect if your presentation looks neat. 
Never skimp on quality.</More>) 
So what can you do?

One solution is to use an off the shelf template.

We decided to use [beautiful.ai](https://beautiful.ai). They propose a selection of customizable slides look good out of the box. The trade off is that their system is constraining. There were a few slides that we simply couldn't make as we wished...

But that's a _good thing_, as it forced us to stay focused.

Our goal was to be clear, and get to the point. Publishers, like most of us, don't have time to go through mountains of text. They also receive a lot of game decks, and may not have a ton of time for your game specifically.

Make sure whomever is reading your deck can quickly understand what your game is about. Think about if you didn't know anything about your game. Would the presentation make sense?

# What Should the Content be?

Publishers are industry people, like you. They don't want to just hear about the hype and marketing language.

A publisher is mainly interested in one question:

**Is your game worth investing time and money into?**

This question can be rephrased as:

**Is your game likely to make money?**

To assess that, a publisher (or anyone, really), needs to understand the following:

1. [What makes your game attractive?](#what-makes-your-game-attractive)
2. [What is the scope of your game?](#what-is-the-scope-of-your-game)
3. [How much money do you need?](#how-much-money-do-you-need)
4. [How much money can you make?](#how-much-money-can-you-make)
4. [Who is your team?](#who-is-your-team)

Let's dive in and explore these points below.

### What makes your game attractive?

People say "what makes your game special?".
I don't think it's a very good way of putting it. Not all successes have something obviously "special". Often gamers go for something _familiar and comforting_, but with attractive graphics, or music, or maybe an appealling narrative. 

So, this is not about what makes you _special_, this is what makes you _likeable_. 

What game are you proposing to make? This _may_ be something that stands out (it is in our case), but it also _may be_ something as simple as "There haven't been Tamagotchi games in a long time, and our monsters' design is extremely cute".

Don't just pitch your dream- pitch how you will execute on it. A tried and true formula will sound more solid than some fake attempt at novelty. Be clear, direct, and honest.

Side note: Not all publishers publish all types of content. Pick publishers that _may_ find your game attractive.

### What is the scope of your game?

In order to assess if your game is even _doable_, a publisher needs the following two pieces of information:

1. How big is your game? and
2. How much of it is done?

For this, it's good to list some metrics, such as:

- estimated hours of play
- estimated amount of enemies
- estimated amount of levels/map size
- estimated amount of powerups
- estimated amount of conversations
- estimated amount of cutscenes
- ... and so on

This list is not exhaustive- you should determine what can help people estimate the breadth of your game. Try to condense this information in easily understood metrics, that can be perused at a glance.

If any significant part of your game is done, indicate it clearly.

### How much money do you need?

What's your burn rate? A publisher will need to know your current costs. Coupled with your scope estimation, this tells the publisher how much money they stand to invest.

Do not include marketing costs and other costs that a publisher will handle. They mostly are concerned about development costs.
<More title="What if I'm a lone developer?">

Even if you're on your own, you presumably live somewhere, and eat something. You also probably wear clothes, and have some kind of electricity, water, and internet bill. Those are _costs_.

Someone (parent, spouse, your savings maybe) is handling those costs. If you want to continue working on your game, you will also need some insurance, some spending money, and so on.

_TL,DR_: even on your own, assign to yourself some salary.

Besides, even if you're a jack of all trades, it's likely that you will need to hire some people. Factor that in.

</More>

If you don't need funding at all that's great, as it lowers the risk on the publisher.

### How Much Money Can You Make?

This is not strictly necessary. As a game developper, you're not really expected to do this. However, including projections is useful for both yourself and your potential publisher.

Estimating game sales is a very difficult exercise. Do rely on all the information you can find. If you've started selling anything, put that number in. If you have followers on social media, or on Discord, put those numbers down too. Wishlist, newsletter subscriptions? Use those.

If you have the numbers for a similar game, include those. If you don't (which is likely), you can use estimates from the amount of Steam comments to determine how much similar games have sold.

If you need some guidance on how to determine your possible revenue, head to our previous blog post, [How much can your game make](/blog/how-much-can-your-game-make/)

### Who is your team?

Lastly, a publisher will need to know if you can bring a project to completion. Most people _can't_.

The vast majority of projects never finish because people lack the consistency necessary. For a publisher to trust you, list:

- who your team members are
- previous accomplishments of each team member

Sell your team members and yourself the best way possible. Don't lie, but don't forget anything, don't leave anything aside.

## I have my deck, now what?

Scour the internet, and get the names of publishers. Look at games that are similar to yours, and find who published them.

Once you have a list, write a short, succint, simple email. Here's ours:


> Greetings!
> 
> [Transmogrify](https://www.playtransmogrify.com/demos) is an unusual 2D puzzle platformer that blends **challenging action** and **handcrafted puzzles**. It’s set in a sci-fi world with comics-like modern graphics, and features a peculiar, **non-lethal weapon that transforms enemies** into tools and structures needed to solve the levels.
> 
> It’s a puzzle game where you need to react quickly, and an action platformer game where you have to think!
> 
> ![a short gif showing the transmogrify gameplay](./transmogrify_gameplay.gif)
> 
> Check out the trailer: [Watch the new trailer on YouTube](https://youtu.be/hbRExts77Bw)
> 
> Play the demo: [Download the new demo](https://www.playtransmogrify.com/demos)
> 
> If this sounds right up your alley: [View the new slide deck](https://drive.google.com/file/d/1w7h3oguc-__GQ2DIZbll5-dZSYgeKVHQ/view)
> 
> Transmogrify:
> - Is an unusual 2D puzzle platformer that blends challenging action, handcrafted puzzles, and a mysterious story
> - Follows Chris and GRACE, the facility AI, as they try to unravel what happened
> - Features a gun that turns enemies into power-ups and tools to solve puzzles
> - Combines a modern comic-book style with sci-fi for an attractive art style
> - Will be fully voice-acted for all characters
> - Will release on Windows, Mac, Linux
> - Supports controller and keyboard
> Learn more with our [presskit](https://www.playtransmogrify.com/presskit) or by checking out the [media slide deck](https://drive.google.com/file/d/1w7h3oguc-__GQ2DIZbll5-dZSYgeKVHQ/view?usp=sharing).
> 
> You can contact us by responding to this email, via [Discord](https://discordapp.com/invite/5AeaZxX), or calling `[redacted]` if you have any questions.
> 
> Thanks for your time!
> 
> Best,
> Andrew and Team Transmogrify
> 
> --
> 
> [Website](https://playtransmogrify.com/)  
> [Trailer](https://youtu.be/hbRExts77Bw)  
> [Steam](https://store.steampowered.com/app/740310/Transmogrify/)  
> [Discord](https://discordapp.com/invite/5AeaZxX)  
> [Itch.io](https://odysseyentertainment.itch.io/transmogrify)  

Note a few details:

1. On top of the trailer, we made a mini-mini trailer as a gif. We compressed it so it doesn't clog people's inboxes, but kept it just good and long enough for the publisher to know, at a glance, if they're interested.
2. We kept our text, short, to the point, well spaced out, and emphasized what we wanted to say in bold, helping a busy reader peruse the email fast.
3. We included all the relevant links to know more.

### In Conclusion

Preparing a game pitch can be difficult and a lot of work. It does really help you take a hard look at your game though and re-focus on your core ideas.

Even if you don't end up getting a publisher, it is a useful exercise that makes you think about how people will perceive your game.
We ended up deciding to self-publish Transmogrify on PC/MacOS/Linux for example, but are considering publishers for consoles.
We still learned a lot, refined our marketing message, and made a lot of great publisher contacts throughout this process.

Hopefully this was helpful, and best of luck with your publisher quest!