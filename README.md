# Transmogrify Website

Built with [Gatsby](https://www.gatsbyjs.org).

## Run

- clone this repo
- `yarn install` (`npm install` should work, but on my machine it didn't)
- test by running `yarn start`
- build by running `yarn run build`
- OR `yarn run develop`

**note**: The presskit link won't work in local tests, because it gets generated at build time

## Use

- add pages in `src/pages`. You can use `mdx` for Markdown pages in which you can still use components. You can use `.js` or `.tsx` for regular React components.
- add images in `src/images`

## Deploy

- push to `master`

## Track

Google Analytics and Google Tag Manager are set up in `gatsby-config.js`.

Plugin documentation is here:

- [gatsby-plugin-google-analytics](https://www.gatsbyjs.org/packages/gatsby-plugin-google-analytics/)
- [gatsby-plugin-google-tagmanager](https://www.gatsbyjs.org/packages/gatsby-plugin-google-tagmanager/)

## Change copy

- Might change in the future, but at the moment, homepage copy is in [./src/homepage-parts/texts.ts](./src/homepage-parts/texts.ts).
- Menu items are in [./src/components/Header.tsx](./src/components/Header.tsx)
- Buttons (both at the top and on the homepage) are defined in [./src/components/icons/index.ts](./src/components/icons/index.ts)
- Images are loaded from [./src/images/presskit](./src/images/presskit), and sorted by file name 

## Deploy To a Domain

- set custom domain as described [here](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/)
- in `gatsby-config.js`:
  - change `pathPrefix` to `/`
  - change `bugTrackerURL` to a bug tracker url (BitBucket or Gitlab)
- in `src/homepage-parts/TheGame.tsx`:
  - change `YOUTUBE_ID` to the youtube video id
